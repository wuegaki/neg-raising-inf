
function generateRandomNumber(){
    var num = Math.floor(Math.random()*10+10);
    var myArray = ['R', 'N', 'B'];
    var rand = myArray[Math.floor(Math.random() * myArray.length)];
    var myArray2 = ['A', 'W', 'E'];
    var rand2 = myArray2[Math.floor(Math.random() * myArray2.length)];
    var myArray3 = ['Q', 'M', 'N'];
    var rand3 = myArray3[Math.floor(Math.random() * myArray3.length)];
    var num2 = Math.floor(Math.random()*10+20);

    var n = String(rand)+String(num)+String(rand3)+String(num2)+String(rand2);

return n;
}

//var shuffleSequence = seq("consent","intro", randomize(seq(startsWith("IE"),startsWith("SE"),startsWith("WE"))),"questionnaire");
var shuffleSequence = seq("intro",rshuffle(endsWith("1"),endsWith("2")),'questionnaire');
    
    
var practiceItemTypes = ["practice"];

var showProgressBar = true;
var pageTitle = "Mechanical Turk Experiment";
// var completionMessage = "The results were successfully sent to the server. You can now validate your participation on Mechanical Turk. Thanks!";
var completionMessage = "The results were successfully sent to the server. You can now validate your participation on Mechanical Turk by entering the following survey code: \n" + generateRandomNumber();
var completionErrorMessage = "There was an error sending the results to the server. Do not refresh this page. Check your internet connection and when it is up again, try sending the results again. ";

var defaults = [
    "Separator", {
        transfer: 350,
        normalMessage: "+",
        errorMessage: "+",
        ignoreFailure: true
    },
    "SeparatorTer", {
        normalTransfer: 350,
        errorTransfer: 1000,
        normalMessage: "Correct!",
        errorMessage: "Wrong!",
        ignoreFailure: false
    },
    "SeparatorBis", {
        normalTransfer: 350,
        errorTransfer: 1000,
        normalMessage: "+",
        errorMessage: "+",
        ignoreFailure: true
    },
    "Scale2", {
        //scaleWidth: 1000,
        scaleHeight: 15,
        handleWidth: 15,
        handleHeight: 40, //-- the width/height of the "handle" which the user slides along the bar.
        //html -- html displayed above the bar (probably specifying the sentence to be rated, in most cases).
        startColor: "#00A0F0",
        endColor: "#00A0F0",
        startValue: 0,
        endValue: 100,
        decimalPlaces: 0,
        scaleLabels: true,
        LeftLab: "B does not follow from A",
        RightLab: "B follows from A",
        hideProgressBar: false
    },
    "DashedSentence", {
        mode: "speeded acceptability",
        display: "in place"
    },
    "Message", {
        hideProgressBar: false,
        transfer: "keypress"
    },
    "Form", {
        hideProgressBar: true,
        continueOnReturn: true
    },
];


var items = [



    ["sep", "Separator", { }],
    ["sepFB", "Separator", {normalMessage:"Correct",errorMessage:"Wrong",transfer:1500,ignoreFailure:false}],

    // New in Ibex 0.3-beta19. You can now determine the point in the experiment at which the counter
    // for latin square designs will be updated. (Previously, this was always updated upon completion
    // of the experiment.) To do this, insert the special '__SetCounter__' controller at the desired
    // point in your running order. If given no options, the counter is incremented by one. If given
    // an 'inc' option, the counter is incremented by the specified amount. If given a 'set' option,
    // the counter is set to the given number. (E.g., { set: 100 }, { inc: -1 })
    //
    //["setcounter", "__SetCounter__", { }],

    // NOTE: You could also use the 'Message' controller for the experiment intro (this provides a simple
    // consent checkbox).

    ["intro", "Form", {
        html: { include: "instructions.html" },
        validators: {
            age: function (s) { if (s.match(/^\d+$/)) return true; else return "Bad value for \u2018age\u2019"; }
        }
    } ],
                
                ["questionnaire", "Form", {
        html: { include: "questionnaire.html" },
        continueMessage: "Click here to submit your results"
    } ],
    
    ["consent", "Message", {
        html: { include: "consent.html" },
        transfer: "click",
        consentRequired: true,
        continueMessage: "Click here to start the experiment",
        consentMessage: "I agree to participate.",
    } ],

['establish-q-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John established which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John established which of his brothers wouldn't come to his birthday party.</p>"}],
['establish-n-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't establish that Bill would come to his birthday party.</p><p class='Sen2'><b>B: </b>John established that Bill wouldn't come to his birthday party.</p>"}],
['establish-consis-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John established which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John established which of his sisters would come to his birthday party.</p>"}],
['establish-contra-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John established which of his brothers and which of his sisters would come to his birthday party.</p><p class='Sen2'><b>B: </b>John established which of his brothers would come to his birthday party.</p>"}],

['report-q-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John reported which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John reported which of his brothers wouldn't come to his birthday party.</p>"}],
['report-n-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't report that Bill would come to his birthday party.</p><p class='Sen2'><b>B: </b>John reported that Bill wouldn't come to his birthday party.</p>"}],
['report-consis-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John reported which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John reported which of his sisters would come to his birthday party.</p>"}],
['report-contra-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John reported which of his brothers and which of his sisters would come to his birthday party.</p><p class='Sen2'><b>B: </b>John reported which of his brothers would come to his birthday party.</p>"}],

['guess-q-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John guessed which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John guessed which of his brothers wouldn't come to his birthday party.</p>"}],
['guess-n-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't guess that Bill would come to his birthday party.</p><p class='Sen2'><b>B: </b>John guessed that Bill wouldn't come to his birthday party.</p>"}],
['guess-consis-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John guessed which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John guessed which of his sisters would come to his birthday party.</p>"}],
['guess-contra-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John guessed which of his brothers and which of his sisters would come to his birthday party.</p><p class='Sen2'><b>B: </b>John guessed which of his brothers would come to his birthday party.</p>"}],

['estimate-q-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John estimated which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John estimated which of his brothers wouldn't come to his birthday party.</p>"}],
['estimate-n-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't estimate that Bill would come to his birthday party.</p><p class='Sen2'><b>B: </b>John estimated that Bill wouldn't come to his birthday party.</p>"}],
['estimate-consis-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John estimated which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John estimated which of his sisters would come to his birthday party.</p>"}],
['estimate-contra-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John estimated which of his brothers and which of his sisters would come to his birthday party.</p><p class='Sen2'><b>B: </b>John estimated which of his brothers would come to his birthday party.</p>"}],

['conjecture-q-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John conjectured which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John conjectured which of his brothers wouldn't come to his birthday party.</p>"}],
['conjecture-n-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't conjecture that Bill would come to his birthday party.</p><p class='Sen2'><b>B: </b>John conjectured that Bill wouldn't come to his birthday party.</p>"}],
['conjecture-consis-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John conjectured which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John conjectured which of his sisters would come to his birthday party.</p>"}],
['conjecture-contra-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John conjectured which of his brothers and which of his sisters would come to his birthday party.</p><p class='Sen2'><b>B: </b>John conjectured which of his brothers would come to his birthday party.</p>"}],

['predict-q-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John predicted which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John predicted which of his brothers wouldn't come to his birthday party.</p>"}],
['predict-n-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't predict that Bill would come to his birthday party.</p><p class='Sen2'><b>B: </b>John predicted that Bill wouldn't come to his birthday party.</p>"}],
['predict-consis-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John predicted which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John predicted which of his sisters would come to his birthday party.</p>"}],
['predict-contra-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John predicted which of his brothers and which of his sisters would come to his birthday party.</p><p class='Sen2'><b>B: </b>John predicted which of his brothers would come to his birthday party.</p>"}],

['tellme-q-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John told me which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John told me which of his brothers wouldn't come to his birthday party.</p>"}],
['tellme-n-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't tell me that Bill would come to his birthday party.</p><p class='Sen2'><b>B: </b>John told me that Bill wouldn't come to his birthday party.</p>"}],
['tellme-consis-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John told me which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John told me which of his sisters would come to his birthday party.</p>"}],
['tellme-contra-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John told me which of his brothers and which of his sisters would come to his birthday party.</p><p class='Sen2'><b>B: </b>John told me which of his brothers would come to his birthday party.</p>"}],

['writedown-q-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John wrote down which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John wrote down which of his brothers wouldn't come to his birthday party.</p>"}],
['writedown-n-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't write down that Bill would come to his birthday party.</p><p class='Sen2'><b>B: </b>John wrote down that Bill wouldn't come to his birthday party.</p>"}],
['writedown-consis-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John wrote down which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John wrote down which of his sisters would come to his birthday party.</p>"}],
['writedown-contra-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John wrote down which of his brothers and which of his sisters would come to his birthday party.</p><p class='Sen2'><b>B: </b>John wrote down which of his brothers would come to his birthday party.</p>"}],

['read-q-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John read which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John read which of his brothers wouldn't come to his birthday party.</p>"}],
['read-n-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't read that Bill would come to his birthday party.</p><p class='Sen2'><b>B: </b>John read that Bill wouldn't come to his birthday party.</p>"}],
['read-consis-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John read which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John read which of his sisters would come to his birthday party.</p>"}],
['read-contra-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John read which of his brothers and which of his sisters would come to his birthday party.</p><p class='Sen2'><b>B: </b>John read which of his brothers would come to his birthday party.</p>"}],

['announce-q-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John announced which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John announced which of his brothers wouldn't come to his birthday party.</p>"}],
['announce-n-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't announce that Bill would come to his birthday party.</p><p class='Sen2'><b>B: </b>John announced that Bill wouldn't come to his birthday party.</p>"}],
['announce-consis-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John announced which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John announced which of his sisters would come to his birthday party.</p>"}],
['announce-contra-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John announced which of his brothers and which of his sisters would come to his birthday party.</p><p class='Sen2'><b>B: </b>John announced which of his brothers would come to his birthday party.</p>"}],

['indicate-q-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John indicated which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John indicated which of his brothers wouldn't come to his birthday party.</p>"}],
['indicate-n-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't indicate that Bill would come to his birthday party.</p><p class='Sen2'><b>B: </b>John indicated that Bill wouldn't come to his birthday party.</p>"}],
['indicate-consis-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John indicated which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John indicated which of his sisters would come to his birthday party.</p>"}],
['indicate-contra-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John indicated which of his brothers and which of his sisters would come to his birthday party.</p><p class='Sen2'><b>B: </b>John indicated which of his brothers would come to his birthday party.</p>"}],

['informme-q-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John informed me which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John informed me which of his brothers wouldn't come to his birthday party.</p>"}],
['informme-n-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't inform me that Bill would come to his birthday party.</p><p class='Sen2'><b>B: </b>John informed me that Bill wouldn't come to his birthday party.</p>"}],
['informme-consis-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John informed me which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John informed me which of his sisters would come to his birthday party.</p>"}],
['informme-contra-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John informed me which of his brothers and which of his sisters would come to his birthday party.</p><p class='Sen2'><b>B: </b>John informed me which of his brothers would come to his birthday party.</p>"}],

['teachme-q-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John taught me which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John taught me which of his brothers wouldn't come to his birthday party.</p>"}],
['teachme-n-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't teach me that Bill would come to his birthday party.</p><p class='Sen2'><b>B: </b>John taught me that Bill wouldn't come to his birthday party.</p>"}],
['teachme-consis-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John taught me which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John taught me which of his sisters would come to his birthday party.</p>"}],
['teachme-contra-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John taught me which of his brothers and which of his sisters would come to his birthday party.</p><p class='Sen2'><b>B: </b>John taught me which of his brothers would come to his birthday party.</p>"}],

['emailme-q-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John emailed me which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John emailed me which of his brothers wouldn't come to his birthday party.</p>"}],
['emailme-n-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't email me that Bill would come to his birthday party.</p><p class='Sen2'><b>B: </b>John emailed me that Bill wouldn't come to his birthday party.</p>"}],
['emailme-consis-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John emailed me which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John emailed me which of his sisters would come to his birthday party.</p>"}],
['emailme-contra-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John emailed me which of his brothers and which of his sisters would come to his birthday party.</p><p class='Sen2'><b>B: </b>John emailed me which of his brothers would come to his birthday party.</p>"}],

['faxme-q-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John faxed me which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John faxed me which of his brothers wouldn't come to his birthday party.</p>"}],
['faxme-n-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't fax me that Bill would come to his birthday party.</p><p class='Sen2'><b>B: </b>John faxed me that Bill wouldn't come to his birthday party.</p>"}],
['faxme-consis-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John faxed me which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John faxed me which of his sisters would come to his birthday party.</p>"}],
['faxme-contra-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John faxed me which of his brothers and which of his sisters would come to his birthday party.</p><p class='Sen2'><b>B: </b>John faxed me which of his brothers would come to his birthday party.</p>"}],

['relaytome-q-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John relayed to me which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John relayed to me which of his brothers wouldn't come to his birthday party.</p>"}],
['relaytome-n-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't relay to me that Bill would come to his birthday party.</p><p class='Sen2'><b>B: </b>John relayed to me that Bill wouldn't come to his birthday party.</p>"}],
['relaytome-consis-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John relayed to me which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John relayed to me which of his sisters would come to his birthday party.</p>"}],
['relaytome-contra-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John relayed to me which of his brothers and which of his sisters would come to his birthday party.</p><p class='Sen2'><b>B: </b>John relayed to me which of his brothers would come to his birthday party.</p>"}],

['signaltome-q-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John signaled to me which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John signaled to me which of his brothers wouldn't come to his birthday party.</p>"}],
['signaltome-n-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't signal to me that Bill would come to his birthday party.</p><p class='Sen2'><b>B: </b>John signaled to me that Bill wouldn't come to his birthday party.</p>"}],
['signaltome-consis-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John signaled to me which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John signaled to me which of his sisters would come to his birthday party.</p>"}],
['signaltome-contra-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John signaled to me which of his brothers and which of his sisters would come to his birthday party.</p><p class='Sen2'><b>B: </b>John signaled to me which of his brothers would come to his birthday party.</p>"}],

['record-q-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John recorded which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John recorded which of his brothers wouldn't come to his birthday party.</p>"}],
['record-n-1', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't record that Bill would come to his birthday party.</p><p class='Sen2'><b>B: </b>John recorded that Bill wouldn't come to his birthday party.</p>"}],
['record-consis-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John recorded which of his brothers would come to his birthday party.</p><p class='Sen2'><b>B: </b>John recorded which of his sisters would come to his birthday party.</p>"}],
['record-contra-1', "Scale2", {html:"<p class='Sen1'><b>A: </b>John recorded which of his brothers and which of his sisters would come to his birthday party.</p><p class='Sen2'><b>B: </b>John recorded which of his brothers would come to his birthday party.</p>"}],

['establish-q-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John established which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John established which of his children wouldn't inherit his fortune.</p>"}],
['establish-n-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't establish that Ann would inherit his fortune.</p><p class='Sen2'><b>B: </b>John established that Ann wouldn't inherit his fortune.</p>"}],
['establish-consis-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John established which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John established which of his brothers would inherit his fortune.</p>"}],
['establish-contra-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John established which of his children and which of his brothers would inherit his fortune.</p><p class='Sen2'><b>B: </b>John established which of his children would inherit his fortune.</p>"}],

['report-q-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John reported which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John reported which of his children wouldn't inherit his fortune.</p>"}],
['report-n-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't report that Ann would inherit his fortune.</p><p class='Sen2'><b>B: </b>John reported that Ann wouldn't inherit his fortune.</p>"}],
['report-consis-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John reported which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John reported which of his brothers would inherit his fortune.</p>"}],
['report-contra-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John reported which of his children and which of his brothers would inherit his fortune.</p><p class='Sen2'><b>B: </b>John reported which of his children would inherit his fortune.</p>"}],

['guess-q-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John guessed which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John guessed which of his children wouldn't inherit his fortune.</p>"}],
['guess-n-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't guess that Ann would inherit his fortune.</p><p class='Sen2'><b>B: </b>John guessed that Ann wouldn't inherit his fortune.</p>"}],
['guess-consis-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John guessed which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John guessed which of his brothers would inherit his fortune.</p>"}],
['guess-contra-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John guessed which of his children and which of his brothers would inherit his fortune.</p><p class='Sen2'><b>B: </b>John guessed which of his children would inherit his fortune.</p>"}],

['estimate-q-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John estimated which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John estimated which of his children wouldn't inherit his fortune.</p>"}],
['estimate-n-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't estimate that Ann would inherit his fortune.</p><p class='Sen2'><b>B: </b>John estimated that Ann wouldn't inherit his fortune.</p>"}],
['estimate-consis-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John estimated which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John estimated which of his brothers would inherit his fortune.</p>"}],
['estimate-contra-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John estimated which of his children and which of his brothers would inherit his fortune.</p><p class='Sen2'><b>B: </b>John estimated which of his children would inherit his fortune.</p>"}],

['conjecture-q-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John conjectured which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John conjectured which of his children wouldn't inherit his fortune.</p>"}],
['conjecture-n-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't conjecture that Ann would inherit his fortune.</p><p class='Sen2'><b>B: </b>John conjectured that Ann wouldn't inherit his fortune.</p>"}],
['conjecture-consis-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John conjectured which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John conjectured which of his brothers would inherit his fortune.</p>"}],
['conjecture-contra-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John conjectured which of his children and which of his brothers would inherit his fortune.</p><p class='Sen2'><b>B: </b>John conjectured which of his children would inherit his fortune.</p>"}],

['predict-q-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John predicted which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John predicted which of his children wouldn't inherit his fortune.</p>"}],
['predict-n-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't predict that Ann would inherit his fortune.</p><p class='Sen2'><b>B: </b>John predicted that Ann wouldn't inherit his fortune.</p>"}],
['predict-consis-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John predicted which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John predicted which of his brothers would inherit his fortune.</p>"}],
['predict-contra-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John predicted which of his children and which of his brothers would inherit his fortune.</p><p class='Sen2'><b>B: </b>John predicted which of his children would inherit his fortune.</p>"}],

['tellme-q-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John told me which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John told me which of his children wouldn't inherit his fortune.</p>"}],
['tellme-n-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't tell me that Ann would inherit his fortune.</p><p class='Sen2'><b>B: </b>John told me that Ann wouldn't inherit his fortune.</p>"}],
['tellme-consis-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John told me which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John told me which of his brothers would inherit his fortune.</p>"}],
['tellme-contra-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John told me which of his children and which of his brothers would inherit his fortune.</p><p class='Sen2'><b>B: </b>John told me which of his children would inherit his fortune.</p>"}],

['writedown-q-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John wrote down which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John wrote down which of his children wouldn't inherit his fortune.</p>"}],
['writedown-n-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't write down that Ann would inherit his fortune.</p><p class='Sen2'><b>B: </b>John wrote down that Ann wouldn't inherit his fortune.</p>"}],
['writedown-consis-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John wrote down which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John wrote down which of his brothers would inherit his fortune.</p>"}],
['writedown-contra-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John wrote down which of his children and which of his brothers would inherit his fortune.</p><p class='Sen2'><b>B: </b>John wrote down which of his children would inherit his fortune.</p>"}],

['read-q-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John read which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John read which of his children wouldn't inherit his fortune.</p>"}],
['read-n-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't read that Ann would inherit his fortune.</p><p class='Sen2'><b>B: </b>John read that Ann wouldn't inherit his fortune.</p>"}],
['read-consis-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John read which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John read which of his brothers would inherit his fortune.</p>"}],
['read-contra-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John read which of his children and which of his brothers would inherit his fortune.</p><p class='Sen2'><b>B: </b>John read which of his children would inherit his fortune.</p>"}],

['announce-q-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John announced which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John announced which of his children wouldn't inherit his fortune.</p>"}],
['announce-n-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't announce that Ann would inherit his fortune.</p><p class='Sen2'><b>B: </b>John announced that Ann wouldn't inherit his fortune.</p>"}],
['announce-consis-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John announced which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John announced which of his brothers would inherit his fortune.</p>"}],
['announce-contra-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John announced which of his children and which of his brothers would inherit his fortune.</p><p class='Sen2'><b>B: </b>John announced which of his children would inherit his fortune.</p>"}],

['indicate-q-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John indicated which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John indicated which of his children wouldn't inherit his fortune.</p>"}],
['indicate-n-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't indicate that Ann would inherit his fortune.</p><p class='Sen2'><b>B: </b>John indicated that Ann wouldn't inherit his fortune.</p>"}],
['indicate-consis-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John indicated which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John indicated which of his brothers would inherit his fortune.</p>"}],
['indicate-contra-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John indicated which of his children and which of his brothers would inherit his fortune.</p><p class='Sen2'><b>B: </b>John indicated which of his children would inherit his fortune.</p>"}],

['informme-q-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John informed me which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John informed me which of his children wouldn't inherit his fortune.</p>"}],
['informme-n-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't inform me that Ann would inherit his fortune.</p><p class='Sen2'><b>B: </b>John informed me that Ann wouldn't inherit his fortune.</p>"}],
['informme-consis-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John informed me which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John informed me which of his brothers would inherit his fortune.</p>"}],
['informme-contra-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John informed me which of his children and which of his brothers would inherit his fortune.</p><p class='Sen2'><b>B: </b>John informed me which of his children would inherit his fortune.</p>"}],

['teachme-q-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John taught me which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John taught me which of his children wouldn't inherit his fortune.</p>"}],
['teachme-n-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't teach me that Ann would inherit his fortune.</p><p class='Sen2'><b>B: </b>John taught me that Ann wouldn't inherit his fortune.</p>"}],
['teachme-consis-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John taught me which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John taught me which of his brothers would inherit his fortune.</p>"}],
['teachme-contra-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John taught me which of his children and which of his brothers would inherit his fortune.</p><p class='Sen2'><b>B: </b>John taught me which of his children would inherit his fortune.</p>"}],

['emailme-q-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John emailed me which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John emailed me which of his children wouldn't inherit his fortune.</p>"}],
['emailme-n-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't email me that Ann would inherit his fortune.</p><p class='Sen2'><b>B: </b>John emailed me that Ann wouldn't inherit his fortune.</p>"}],
['emailme-consis-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John emailed me which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John emailed me which of his brothers would inherit his fortune.</p>"}],
['emailme-contra-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John emailed me which of his children and which of his brothers would inherit his fortune.</p><p class='Sen2'><b>B: </b>John emailed me which of his children would inherit his fortune.</p>"}],

['faxme-q-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John faxed me which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John faxed me which of his children wouldn't inherit his fortune.</p>"}],
['faxme-n-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't fax me that Ann would inherit his fortune.</p><p class='Sen2'><b>B: </b>John faxed me that Ann wouldn't inherit his fortune.</p>"}],
['faxme-consis-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John faxed me which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John faxed me which of his brothers would inherit his fortune.</p>"}],
['faxme-contra-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John faxed me which of his children and which of his brothers would inherit his fortune.</p><p class='Sen2'><b>B: </b>John faxed me which of his children would inherit his fortune.</p>"}],

['relaytome-q-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John relayed to me which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John relayed to me which of his children wouldn't inherit his fortune.</p>"}],
['relaytome-n-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't relay to me that Ann would inherit his fortune.</p><p class='Sen2'><b>B: </b>John relayed to me that Ann wouldn't inherit his fortune.</p>"}],
['relaytome-consis-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John relayed to me which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John relayed to me which of his brothers would inherit his fortune.</p>"}],
['relaytome-contra-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John relayed to me which of his children and which of his brothers would inherit his fortune.</p><p class='Sen2'><b>B: </b>John relayed to me which of his children would inherit his fortune.</p>"}],

['signaltome-q-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John signaled to me which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John signaled to me which of his children wouldn't inherit his fortune.</p>"}],
['signaltome-n-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't signal to me that Ann would inherit his fortune.</p><p class='Sen2'><b>B: </b>John signaled to me that Ann wouldn't inherit his fortune.</p>"}],
['signaltome-consis-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John signaled to me which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John signaled to me which of his brothers would inherit his fortune.</p>"}],
['signaltome-contra-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John signaled to me which of his children and which of his brothers would inherit his fortune.</p><p class='Sen2'><b>B: </b>John signaled to me which of his children would inherit his fortune.</p>"}],

['record-q-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John recorded which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John recorded which of his children wouldn't inherit his fortune.</p>"}],
['record-n-2', 'Scale2', {html:"<p class='Sen1'><b>A: </b>John didn't record that Ann would inherit his fortune.</p><p class='Sen2'><b>B: </b>John recorded that Ann wouldn't inherit his fortune.</p>"}],
['record-consis-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John recorded which of his children would inherit his fortune.</p><p class='Sen2'><b>B: </b>John recorded which of his brothers would inherit his fortune.</p>"}],
['record-contra-2', "Scale2", {html:"<p class='Sen1'><b>A: </b>John recorded which of his children and which of his brothers would inherit his fortune.</p><p class='Sen2'><b>B: </b>John recorded which of his children would inherit his fortune.</p>"}]

    
 ];

    
    
