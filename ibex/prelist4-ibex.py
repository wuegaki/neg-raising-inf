#!/usr/bin/python
# coding: UTF-8

filler_number = 0
target_number = {}
already = []
counter2 = 1
# verb_group1 = ["announce","conjecture","establish","report","writedown","read","relaytome","guess","informme"]

def func(arg):
	import __main__
	arg = map(lambda x:x.strip(' '),arg)
	dummy1,dummy2,verb_inf,verb_past,dom,VP,name1,dom2,name2 = arg
	# print arg
	# print target
	q1 = "John %s which of %s would %s." % (verb_past, dom, VP)
	# print q1
	q2 = "John %s which of %s wouldn't %s." %(verb_past, dom, VP)
	n1 = "John didn't %s that %s would %s." % (verb_inf, name1, VP)
	n2 = "John %s that %s wouldn't %s." % (verb_past, name1, VP)
	consis1 = "John %s which of %s would %s." % (verb_past,dom,VP)
	consis2 = "John %s which of %s would %s." % (verb_past,dom2,VP)
	contra1 = "John %s which of %s and which of %s would %s." %(verb_past,dom,dom2,VP)
	contra2 = "John %s which of %s would %s." %(verb_past,dom,VP)
	# q_f1 = "John %s which of %s would %s. %s is one of %s." % (verb_past,dom,VP,name2,dom)
	# contr_2f = "John %s that %s wouldn't %s." % (verb_past,name2,VP)
	# q_consis = "John %s which of %s would %s, and he didn't %s which of %s would." %(verb_past,dom,VP,verb_inf,dom2)
	# n_contra = "John %s that %s would %s, and he %s that %s wouldn't." %(verb_past,name1,VP,verb_past,name1)
	# n_consis = "John %s that %s would %s, and he %s that %s wouldn't." %(verb_past,name1,VP,verb_past,name2)
	# print already
	# if target == "no":
	# 	__main__.filler_number += 1
	# 	filter_flag = "# filler %d filler" %filler_number
	# 	# print filter_flag
	# 	filter_sent = "%s" %filler
	# 	return filter_flag + "\n" + filter_sent
	# if target == "yes":
		# print "This"
	# if verb_inf in already:
	# 	__main__.target_number[verb_inf] += 1
	# else:
	# 	__main__.already.append(verb_inf)
	# 	__main__.target_number[verb_inf] = 1
	verb_name = verb_inf.replace(' ','')
	# var_number = (target_number[verb_inf] % 36)
	# if var_number == 0:
	# 	var_number = 36
	target_q = "[\'%s-q-%d\', \'Scale2\', {html:\"<p class='Sen1'><b>A: </b>%s</p><p class='Sen2'><b>B: </b>%s</p>\"}]," % (verb_name, counter2, q1, q2)
	# target_sent1 = "%s, and %s" % (q1, q2)
	# target_flag2 = "# target %d %s-n" % (target_number[verb_inf], verb_name) 
	target_n = "[\'%s-n-%d\', \'Scale2\', {html:\"<p class='Sen1'><b>A: </b>%s</p><p class='Sen2'><b>B: </b>%s</p>\"}]," % (verb_name, counter2, n1, n2)
	# target_sent2 = "%s, and %s" % (n1, n2)
	control_consis = "[\'%s-consis-%d\', \"Scale2\", {html:\"<p class='Sen1'><b>A: </b>%s</p><p class='Sen2'><b>B: </b>%s</p>\"}]," % (verb_name, counter2, consis1, consis2)
	control_contra = "[\'%s-contra-%d\', \"Scale2\", {html:\"<p class='Sen1'><b>A: </b>%s</p><p class='Sen2'><b>B: </b>%s</p>\"}]," % (verb_name, counter2, contra1, contra2)
	if verb_name == "record":
		__main__.counter2 += 1
	return target_q + "\n" + target_n + "\n" + control_consis + "\n" + control_contra

# print func(("1",'test2','test3','test4','test5','test6','test7','test8'))


counter = 1
while counter < 2:
	for line in open('input.txt', 'r'):
		# if line != "":
		line = line.rstrip()
		line = line.strip('|')
		line = line.split('|')
	# print line
		print func(tuple(line)) + "\n"
	counter +=1


